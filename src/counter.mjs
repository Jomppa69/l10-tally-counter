import logger from "./logger.mjs";

class Counter {
    constructor(initialValue = 0) {
        this.value = initialValue;
    }

    increase() {
        this.value++;
        logger.log("info",`[COUNTER] increase ${this.value}`)
        return this.value;
    }
    read() {
        logger.log("info",`[COUNTER] read ${this.value}`)
        return this.value;
        
    }
    zero() {
        this.value = 0;
        logger.log("info",`[COUNTER] zero ${this.value}`)
        return this.value;
    }
}

export default Counter;
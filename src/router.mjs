import express from "express";
import Counter from "./counter.mjs";
import logger from "./logger.mjs";
import endpoint_mv from "./endpoint.mw.mjs";

const app = express();

const counter = new Counter();
// Middleware
app.use(endpoint_mv);

// Endpoints
app.get("", (req, res) => res.send("Welcome!"));
// GET /counter-increase
app.get("/counter-increase", (req,res) => {
    res.send(`${counter.increase()}`);
})
// GET /counter-read
app.get("/counter-read", (req,res) => {
    res.send(`${counter.read()}`);
})
// GET /counter-zero
app.get("/counter-zero", (req,res) => {
    res.send(`${counter.zero()}`);
})

// Handle not defined resources/urls
app.all("*", (req, res) => {
    res.status(404).send("Tested non-existing resource, but response status code was not 404.");
    logger.error("this is an error");
})

export default app;